import numpy as np
import argparse

import pyaudio
########################################################################

class audio_input:
    def __init__(self, chunk_size):
        self.chunk_size = chunk_size
        width = 2
        channels = 2
        rate = 48000
        self.p = pyaudio.PyAudio()
        self.stream = self.p.open(
            format=self.p.get_format_from_width(width),
            channels=channels,
            rate=rate,
            input=True,
            output=False,
            frames_per_buffer=chunk_size,
            start=False)
        self.dt = np.dtype([('left', '<i2'), ('right', '<i2')])
        self.stream.start_stream()

    def read_audio(self):
        def get_frames():
            available_frames = self.stream.get_read_available()
            print available_frames
            if available_frames >= self.chunk_size:
                return self.stream.read(available_frames)
        frames = get_frames()
        while not frames:
            frames = get_frames()
        
        return np.fromstring(frames, self.dt)['left'][-self.chunk_size:]
