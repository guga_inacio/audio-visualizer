import numpy
from matplotlib import pyplot

fig = pyplot.figure()
ax = pyplot.axes()
stuff = ax.bar([0, 10, 20, 30], [5, 10, 20, 30], width=10, bottom=None)
for asdf, height in zip(stuff, [30, 20, 10, 5]):
    asdf.set_height(height)
pyplot.show()

