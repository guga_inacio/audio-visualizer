import argparse
import ws2812
import audio
import math

parser = argparse.ArgumentParser(description='Audio visualizer.')
parser.add_argument('--LEDs', type=int, default=20, help='Number of LEDs to use in light show')
parser.add_argument('--peak_gravity', type=float, default=1000, help='How fast the peak decays')
parser.add_argument('--caps', type=int, default=1, help='Show "caps" on peakmeter (1 = on, 0 = off)')
parser.add_argument('--chunk', type=int, default=1024, help='Audio chunk size')
args = parser.parse_args()

BGR_gradient = [[0,0,255],[255,0,255],[255,0,0],[255,255,0],
                [0,255,0]]

cap_color = [100,100,100]

PEAK_MAX = 2**15

def start_output(audio_stream, pixel_output, num_LEDs, gravity, caps):
    pixels = []
    pixel_colors = []
    peak = 0
    pixels = [[0,0,0]]*args.LEDs

    for i in range(num_LEDs):
        pixel_colors.append(ws2812.map_float_to_rgb(
                            float(i)/num_LEDs,BGR_gradient))
    while True:
        frames = audio_stream.read_audio()

        # Handle peaks
        if caps:
            new_max = max(frames)
            if new_max > peak:
                peak = new_max + 2*gravity
            if peak > gravity:
                peak -= gravity
        else:
            new_max = peak = max(frames)

        for i in range(num_LEDs):
            # Calculate pixel brightnesses based on bar position
            if i < new_max*num_LEDs/PEAK_MAX:
                pixels[i] = pixel_colors[i]
            else:
                brightness = 1.0-(i-new_max*num_LEDs/PEAK_MAX)
                if brightness > 0:
                    pixels[i] = [int(x*brightness) for x in pixel_colors[i]]
                else:
                    pixels[i] = [0,0,0]

            # Calculate pixel brightnesses based on cap position
            if caps:
                if abs(i - peak*num_LEDs/PEAK_MAX) < 1:
                    cap_brightness = 1.0 - abs(i - peak*num_LEDs/PEAK_MAX)
                    pixels[i] = [int(bar + cap*cap_brightness) for bar, cap in zip(pixels[i], cap_color)]
                    if max(pixels[i]) > 255:
                        pixels[i] = [x/max(pixels[i]) for x in pixels[i]]

        pixel_output.write(pixels)

stream = audio.audio_input(args.chunk)
output = ws2812.PixelWriter()
start_output(stream, output, args.LEDs, args.peak_gravity, args.caps)
