import argparse
import ws2812
import audio
import math
import time

parser = argparse.ArgumentParser(description='Audio visualizer.')
parser.add_argument('--LEDs', type=int, default=20, help='Number of LEDs to use in light show')
parser.add_argument('--chunk', type=int, default=1024, help='Audio chunk size')
args = parser.parse_args()

BGR_gradient = [[0,0,255], [255, 0, 255], [255,0,0],[255,255,0],
                [0,255,0]]
#BGR_gradient = [[255, 0, 0], [255, 0, 255], [0, 255, 0]]

pixels = [[0,0,0]]*args.LEDs
pixel_brightnesses = [0]*args.LEDs
output_buffer = [[0,0,0]]*args.LEDs

stream = audio.audio_input(args.chunk)
pixel_output = ws2812.PixelWriter()

while True:
    frames = stream.read_audio()
    peak = max(frames)/2.0**15
    rms = math.sqrt(sum([x**2 for x in frames])/len(frames))/2.0**15

    if rms > 0.35:
        pixel_color = ws2812.map_float_to_rgb(peak, BGR_gradient)
        pixel_brightnesses = [1.0]*args.LEDs
    else:
        pixel_color = ws2812.map_float_to_rgb(0.1, BGR_gradient)
        for i in xrange(len(pixel_brightnesses)):
            if pixel_brightnesses[i] > 0.1:
                pixel_brightnesses[i] -= 0.1
            else:
                pixel_brightnesses[i] = 0

    pixels.insert(0, pixel_color)
    pixels.pop()

    for i in xrange(len(pixel_brightnesses)):
        output_buffer[i] = [int(pixel_brightnesses[i]*color) for color in pixels[i]]

    pixel_output.write(output_buffer[::-1]+output_buffer)
    time.sleep(0.06)
